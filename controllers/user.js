'use strict'

//libreria para validar datos  DOC: https://www.npmjs.com/package/validator
const validator = require('validator');
//libreria para encriptar contraseñas
const bcrypt = require('bcrypt-nodejs');
//importamos modelo Usuario
const User = require('../models/user');
//importamos servicio que crea token de usuario
const jwt = require('../services/jwt');

var controller = {
    
    //***Create usuario
    save: function(req, res){
        //get params 
        var params = req.body;

        /*Valores no obligatorios, vacios x default si llegan vacios del form
        params.bornDate = req.body.bornDate || '';
        params.phone = req.body.phone || '';
        params.role = req.body.role || '';
        */
        //validations
        var valid_name= !validator.isEmpty(params.name);
        var valid_surname= !validator.isEmpty(params.surname);
        var valid_nick = !validator.isEmpty(params.nick);
        var valid_email = !validator.isEmpty(params.email);
        var valid_password = !validator.isEmpty(params.password);
        var valid_bornDate = !validator.isEmpty(params.bornDate) ;
        var valid_phone = !validator.isEmpty(params.phone);
        var valid_role = !validator.isEmpty(params.role);
        
        
       
        //console.log(params);
        //console.log(valid_email, valid_password, valid_nick, valid_surname, valid_name);

        //obligatory params;
        if (valid_email && valid_password && valid_nick && valid_surname && valid_name){
            //create object user
            var user = new User();
            
            //values to objects 
            user.email = params.email.trim();
            user.password = params.password;
            user.name = params.name.trim().toLowerCase();
            user.surname = params.surname.toLowerCase();
            user.nick = params.nick;
            user.bornDate = params.bornDate;
            user.phone = params.phone.toString();
            user.role = params.role.trim();

            //optional parameters
            if (valid_bornDate) {
                try{
                    user.bornDate = params.bornDate;
                }catch(err){
                    return res.status (404).send({
                        message: 'Fecha de nacimiento introducida no válida.'
                    });
                }
            }else{
                user.bornDate = " ";
            }
            if (valid_phone) {
                try{
                    user.phone = params.phone.trim().toString();
                }catch(err){
                    return res.status (404).send({
                        message: 'Teléfono introducido no válido.'
                    });
                }
            }else{
                user.phone = " ";
            }
            if (valid_role) {
                try{
                    user.role = params.role.trim().toString();
                }catch(err){
                    return res.status (404).send({
                        message: 'Rol introducido no válido.'
                    });
                }
            }else{
                user.role = "user";
            }
            
            //comprobar si existe el usuario
            User.findOne({email: user.email}, (err, issetUser) => {
                //sino existe
                if (err){
                    
                    return res.status (500).send({
                        message: 'ERROR: AL intentar comprobar duplicidad del usuario.' , 
                        error: err.toString(),
                    });
                }
                
                if (!issetUser){
                    //cifrar contraseña
                    bcrypt.hash(params.password, null, null, (err, hash) =>{
                        
                        user.password = hash;
                    
                        //guardar usuario
                        user.save((err, userSaved) => {
                           
                            if (err){
                                return res.status (500).send({
                                    message: 'ERROR: al guardar el usuario.',
                                    error: err.toString(),
                                });
                            }
                            if(!userSaved){
                                return res.status (400).send({
                                    message: 'No se ha guardado el usuario.',
                                });
                            }
                            //devolver respuesta
                            return res.status (200).send({
                                status: 'success',
                                user: userSaved
                            });
                        });//save
                    });//bcrypt
                    
                }else{
                    return res.status (500).send({
                        message: 'ERROR: Ya existe un usuario registrado con ese correo.'
                    });
                }
            })
        }else{
            return res.status(200).send({
                message: 'Validacion incorrecta, Los datos introducidos no son validos, por favor revíselos. ', params
            }); 
        }

    },// end function save 
    //***Login usuario
    login: function(req, res){
        //Recoger parametros de la peticion
        var params = req.body;
        
        //validar datos
        var valid_email = !validator.isEmpty(params.email) && validator.isEmail(params.email);
        var valid_password = !validator.isEmpty(params.password);
        
        if (!valid_email || !valid_password){ 
            return res.status(200).send({
                message: 'Validacion incorrecta, Los datos introducidos no son validos, por favor revíselos. ', params
            }); 
        }else{
            //comprobar usuario con email indicado
            User.findOne({email: params.email.toLowerCase()}, (err, user) => {
                if (err){
                    return res.status (500).send({
                        message: 'ERROR: Al intentar comprobar credenciales del usuario .'
                    });
                }
                //no encuentra usuario
                if (!user){
                    return res.status (400).send({
                        message: 'El email indicado no está registrado.'
                    });
                }else{
                    //comprobar email/contraseña correcto
                    bcrypt.compare(params.password, user.password, (err, check) => {
                        if(!check){
                            return res.status (400).send({
                                message: 'Contraseña incorrecta.'
                            });
                        }else{
                            //Generar y devolver token (JWT)
                            //si recibimos token de los parametros, es decir, esta generado
                            if(params.getToken){
                                //devolvemos datos (token)
                                return res.status (200).send({
                                    message: 'succes',
                                    token: jwt.createToken(user)
                                });
                            }else{
                                //Limpiamos objeto usuario (para que algunos campos no se devuelvan en la peticion)
                                user.password = undefined;
                                user.__v = undefined;
            
                                //devolver respuesta
                                return res.status (200).send({
                                    message: 'success',
                                    user,
                                    token: jwt.createToken(user)
                                });
                            }
                            
                        }
                    });
                
                }
            });
        } 
    },//end login

    update: function(req, res){
        //recoger datos del usuario
        console.log(req.user);
        var params = req.body;

        //actualizamos datos con los datos introducidos por el usuario, vacios NO se modifican
        if (!validator.isEmpty(params.email)) {         
                params.email =  req.body.email;
        }
        if (!validator.isEmpty(params.nick)) {        
                params.nick =  req.body.nick;
        }
        if (!validator.isEmpty(params.name)) { 
                params.name =  req.body.name;
        }
        if (!validator.isEmpty(params.surname)) {
                params.surname =  req.body.surname ;
        }
        
        if (!validator.isEmpty(params.bornDate)) {
            params.bornDate = req.body.bornDate;
        }
        if (!validator.isEmpty(params.phone)) {
            params.phone = req.body.bornDate;
        }
        if (!validator.isEmpty(params.role)) {
           params.role = req.body.role;
        }

        //Eliminar propiedad de password ** si en un futuro se puede modificar añadir opcion como las anteriores**
        delete params.password;

        //capturamos id del usuario comprobado
        var userId = req.user.sub;

        
        //comprobar que el email es único
        if(!validator.isEmpty(params.email) && req.user.email != params.email){
            //buscamos si existe un usuario con el nuevo email
            User.findOne({email: params.email.toLowerCase()}, (err, user) => {
                if (err){
                    return res.status (500).send({
                        message: 'ERROR: Al buscar usuario mediante email para proseguir a modificar email .',
                        error: err.toString()
                    });
                }
                //encuentra usuario
                if (user && user.email == params.email){
                    return res.status (400).send({
                        message: 'Ese email ya está registrado con otro usuario, por favor indique otro'
                    });
                }else{
                    //buscar y actualizar documento
                    User.findByIdAndUpdate({_id: userId}, params, {new:true}, (err, userUpdated) =>{
                        if(err){
                            return res.status (400).send({
                                status: 'error',
                                message: 'Error al actualizar usuario'
                            });
                        }

                        if(!userUpdated){
                            return res.status (400).send({
                                status: 'error',
                                message: 'Ha ocurrido algún error al actualizar los datos de usuario, por favor vuelva a intentarlo.'
                            });
                        }

                        //devolver respuesta
                        return res.status (200).send({
                            status: 'success',
                            user: userUpdated
                        });
                    });
                }
            });
        }else{
            //buscar y actualizar documento
            User.findByIdAndUpdate({_id: userId}, params, {new:true}, (err, userUpdated) =>{
                if(err){
                    return res.status (400).send({
                        status: 'error',
                        message: 'Error al actualizar usuario'
                    });
                }

                if(!userUpdated){
                    return res.status (400).send({
                        status: 'error',
                        message: 'Ha ocurrido algún error al actualizar los datos de usuario, por favor vuelva a intentarlo.'
                    });
                }

                //devolver respuesta
                return res.status (200).send({
                    status: 'success',
                    user: userUpdated
                });
            });
        }
        
        
    },
    //listar todos los usuarios
    getUsers: function(req, res){
        User.find().exec((err, users) => {
            if(err || !users){
                return res.status (404).send({
                    status: 'error',
                    message: 'No hay usuarios a mostrar.'
                });
            }else{
                var usersList = JSON.stringify(users, null, 2);
                return res.status (200).send({
                    status: 'succes',
                    users
                });
            }
        });
    },
    //obtener usuario mediante su id
    getUser: function(req, res){
        var userId = req.params.userId;
        User.findById(userId).exec((err, user) =>{
            console.log(userId);
            if(err || !user){
                return res.status (404).send({
                    status: 'error',
                    message: 'No hay usuario con ese id.'
                });
            }

            return res.status (200).send({
                status: 'success',
                user
            });
        });
    }

    
};//end controller

module.exports = controller;