'use strict'

//libreria para validar datos  DOC: https://www.npmjs.com/package/validator
const validator = require('validator');
const Film = require('../models/film');
//librerias para trabajar con la subida de ficheros
var fs = require('fs');
var path = require ('path');

var controller = {
    //***Create film
    save: function(req, res){
        //get params 
        var params = req.body;
        //console.log(params);
        
        //validations
        var valid_title= !validator.isEmpty(params.title);
        var valid_synopsis= !validator.isEmpty(params.synopsis);
        var valid_genre = !validator.isEmpty(params.genre);
        var valid_long = !validator.isEmpty(params.long);
        var valid_year = !validator.isEmpty(params.year);
        var valid_colection = !validator.isEmpty(params.colection);
        var valid_director = !validator.isEmpty(params.director);
        var valid_actor = !validator.isEmpty(params.actor);
        var valid_trailer = !validator.isEmpty(params.trailer);
        var valid_img = !validator.isEmpty(params.img);
        var valid_uri = !validator.isEmpty(params.uri);
        var valid_uri2 = !validator.isEmpty(params.uri2);
        var valid_uri3 = !validator.isEmpty(params.uri3);
        var valid_uri4 = !validator.isEmpty(params.uri4);
        var valid_uri5 = !validator.isEmpty(params.uri5);
        /*var valid_seen = !validator.isEmpty(params.seen);*/
            
        //console.log(valid_title, valid_synopsis, valid_img, valid_uri);

        //obligatory params;
        if (valid_title && valid_synopsis && valid_img ){
            //create object user
            var film = new Film();
            
            //values to objects 
            film.title = params.title;
            film.synopsis = params.synopsis;
            film.img = params.img;
            
            //optional parameters
            if (valid_genre) {
                try{
                    film.genre = params.genre;
                }catch(err){
                    return res.status (404).send({
                        message: 'Género de la pelicula incorrecto.'
                    });
                }
            }else{
                film.genre =  ' ';
            }
            if (valid_long) {
                try{
                    film.long = params.long;
                }catch(err){
                    return res.status (404).send({
                        message: 'Duracion pelicula no valida'
                    });
                }
            }else{
                 film.long =  ' ';
            }
            if (valid_year) {
                try{
                    film.year = params.year;
                }catch(err){
                    return res.status (404).send({
                        message: 'Año de la pelicula incorrecto.'
                    });
                }
            }else{
               film.year =  ' ';
            }
            if (valid_colection) {
                try{
                    film.colection = params.colection;
                }catch(err){
                    return res.status (404).send({
                        message: 'Coleccion de la pelicula incorrecta.'
                    });
                }
            }else{
                 film.colection =  ' ';
            }
            if (valid_director) {
                try{
                    film.director = params.director;
                }catch(err){
                    return res.status (404).send({
                        message: 'director de la pelicula incorrecto.'
                    });
                }
            }else{
                film.director =  ' ';
            }
            
            if (valid_actor) {
                try{
                    film.actor = params.actor;
                }catch(err){
                    return res.status (404).send({
                        message: 'Actor de la pelicula incorrecto.'
                    });
                }
            }else{
                film.actor =  ' ';
            }
            if (valid_trailer){
                try{
                    film.trailer = params.trailer;
                }catch(err){
                    return res.status (404).send({
                        message: 'Trailer de la pelicula incorrecto.'
                    });
                }
            }else{
                 film.trailer = ' ';
            }
            if (valid_uri){
                try{
                    film.uri = params.uri;
                }catch(err){
                    return res.status (404).send({
                        message: 'Link de la pelicula incorrecto.'
                    });
                }
            }else{
                 film.uri = ' ';
            }
            if (valid_uri2){
                try{
                    film.uri2 = params.uri2;
                }catch(err){
                    return res.status (404).send({
                        message: 'Segunda opcion link de la pelicula incorrecto.'
                    });
                }
            }else{
                 film.uri2 = ' ';
            }
            if (valid_uri3){
                try{
                    film.uri3 = params.uri3;
                }catch(err){
                    return res.status (404).send({
                        message: 'Tercera opcion link de la pelicula incorrecto.'
                    });
                }
            }else{
                 film.uri3 = ' ';
            }
            if (valid_uri4){
                try{
                    film.uri4 = params.uri4;
                }catch(err){
                    return res.status (404).send({
                        message: 'Cuarta opcion link de la pelicula incorrecto.'
                    });
                }
            }else{
                 film.uri4 = ' ';
            }
            if (valid_uri5){
                try{
                    film.uri5 = params.uri5;
                }catch(err){
                    return res.status (404).send({
                        message: 'Quinta opcion link de la pelicula incorrecto.'
                    });
                }
            }else{
                 film.uri5 = ' ';
            }
            
            //console.log(valid_trailer);
            
            /*delete params.seen;*/
      
            //console.log(film);

            //comprobar si existe pelicula con ese mismo titulo
            Film.findOne({title: film.title}, (err, issetFilm) => {
                //sino existe
                if (err){
                    return res.status (500).send({
                        message: 'ERROR: AL intentar comprobar duplicidad de la pelicula.' , 
                        error: err.toString(),
                    });
                }

                if (!issetFilm){
                    //guardar pelicula
                    film.save((err, filmSaved) => {
                        
                        if (err){
                            return res.status (500).send({
                                message: 'ERROR: al guardar pelicula.',
                                error: err.toString(),
                            });

                        }
                        if(!filmSaved){
                            return res.status (400).send({
                                message: 'No se ha guardado la pelicula.',
                            });
                        }
                        //devolver respuesta
                        return res.status (200).send({
                            status: 'success',
                            film: filmSaved
                        });
                    });//save
                }else{
                    return res.status (500).send({
                        message: 'ERROR: Ya existe una película con ese título, por favor revíselo.'
                    });
                }
            })
         
        }else{
            return res.status(404).send({
                message: 'Validacion incorrecta al guardar pelicula. Los datos obligatorios introducidos no son validos, por favor revíselos. ', params
            }); 
        }
    },// end function save 

    //update film
    update: function(req, res){
        //recoger id peli de la url
        var filmId = req.params.id;
        
        //recogemos parametros para actualizar
        var params = req.body;
        //console.log(params);
        //actualizamos peli con los datos recogidos por post, vacios NO se modifican
        if (!validator.isEmpty(params.title)) {
                params.title = req.body.title;
        }
        if (!validator.isEmpty(params.synopsis)) {
                params.synopsis = req.body.synopsis;
        }
        if (!validator.isEmpty(params.genre)) {
                params.genre = req.body.genre;
        }else{
                params.genre = ' ';
        }
        if (!validator.isEmpty(params.long)) {
                params.long = req.body.long;
        }else{
                params.long = ' ';
        }
        if (!validator.isEmpty(params.year)) {
                params.year = req.body.year;
        }else{
                params.year = ' ';
        }
        if (!validator.isEmpty(params.colection)) {
                params.colection = req.body.colection;
        }else{
                params.colection = ' ';
        }
        if (!validator.isEmpty(params.director)) {
                params.director = req.body.director;
        }else{
                params.director = ' ';
        }
        if (!validator.isEmpty(params.actor)) {
                params.actor = req.body.actor;
        }else{
                params.actor = ' ';
        }
        if (!validator.isEmpty(params.trailer)) {
                params.trailer = req.body.trailer;
        }else{
                params.trailer = ' ';
        }
        if (!validator.isEmpty(params.img)) {
                params.img = req.body.img;
        }
        if (!validator.isEmpty(params.uri)) {
                params.uri = req.body.uri;
        }
        if (!validator.isEmpty(params.uri2)) {
                params.uri2 = req.body.uri2;
        }
        if (!validator.isEmpty(params.uri3)) {
                params.uri3 = req.body.uri3;
        }
        if (!validator.isEmpty(params.uri4)) {
                params.uri4 = req.body.uri4;
        }
        if (!validator.isEmpty(params.uri5)) {
                params.uri5 = req.body.uri5;
        }
        

        //find and update de la peli mediante su id
        Film.findOneAndUpdate({ _id:filmId }, params, {new:true}, (err, filmUpdated) =>{
            if (err){
                return res.status (500).send({
                    message: 'ERROR: error en la peticion de actualizar pelicula.',
                    status: 'error',
                    error: err.toString(),
                });
            }
            if(!filmUpdated){
                return res.status (404).send({
                    status: 'error',
                    message: 'No se ha actualizado la pelicula.',
                });
            }else{
                //devolver respuesta
                return res.status (200).send({
                    status: 'success',
                    filmUpdated 
                });
            }
            
        } )
        
    },//end update

    //listar ultimas 36 pelis
    getFilms: function(req, res){
        //novedades de pelis
        Film.find().sort([['_id', -1]]).limit(36).exec((err, news) => {
            if (err){
                return res.status (500).send({
                    message: 'ERROR: al buscar novedades.',
                    error: err.toString(),
                });
            }
            if(!news){
                errores += 1;
                return res.status (404).send({
                    status: 'error',
                    message: 'No hay pelis novedades a mostrar.'
                });
            }else{
                   return res.status (200).send({
                    status: 'success',
                    news
                });
            }
        });

    },// end getFilms

    //obtener peli mediante su id
    getFilm: function(req, res){
        var filmId = req.params.id;
        Film.findById(filmId).exec((err, film) =>{
            //console.log(req.params);
            if(err || !film){
                return res.status (404).send({
                    status: 'error',
                    message: 'No hay pelicula con ese id.'
                });
            }

            return res.status (200).send({
                status: 'success',
                film
            });
        });
    },

    //eliminar pelicula por id
    delete: function(req, res){
        //recoger id peli de la url
        var filmId = req.params.id

        //find and update de la peli mediante su id
        Film.findOneAndDelete({_id : filmId}, (err, filmRemoved) =>{
            if (err){
                return res.status (500).send({
                    message: 'ERROR: al eliminar pelicula.',
                    error: err.toString(),
                });
            }
            if(!filmRemoved){
                return res.status (400).send({
                    message: 'No se ha eliminado la pelicula.',
                });
            }

            //devolver respuesta
            return res.status (200).send({
                status: 'success',
                film: filmRemoved
            });
        } )
    },// end delete
    //obtener generos usadas por las peliculas
    getGenres: function(req, res){
       
        Film.distinct("genre").exec((err, genres) =>{
            
            if(err || !genres){
                return res.status (404).send({
                    status: 'error',
                    message: 'No hay generos.'
                });
            }

            return res.status (200).send({
                status: 'success',
                genres
            });
        });
    }, 

     //listado de peliculas por genero
    getfilmsByGenre: function(req, res){
        
        var page;
        //console.log(req.params.page);
        
        //recoger pag actual
        if(!req.params.page || req.params.page == 0 || req.params.page == '0' || req.params.page == null || req.params.page == undefined ){
            page = 1;
        }else{
            page = parseInt(req.params.page);
        }
        //opciones de paginacion
        var options = {
            sort: { _id: -1 },
            limit: 16,
            page: page,
            prevPage : page,
            nextPage  : page
        };
        /*
        *****************  PAGINACION todas las pelis (libreria cargada en modelo)
        Film.paginate({}, options, (err, films) =>{
        */
        // 1 - listar pelis
        //recoger genre a buscar (seleccionado en front)
        var genre = req.params.genre
        //console.log(genre);
        
        //Film.find({ genre: genre }).sort([['title', 1]]).exec((err, films) => {
        Film.paginate({ genre: genre }, options, (err, films) => {
            if(err ){
                return res.status (500).send({
                    status: 'error',
                    message: 'Error al consultar peliculas por género',
                    err
                });
            }
        
            if(!films){
                return res.status (404).send({
                    status: 'error',
                    message: 'No hay peliculas a mostrar para el genero ' + genre,
                });
            }else{ 

                //respuesta satisfactoria ( 16 en 16 pelis , total pelis, total paginas)
                return res.status (200).send({
                    status: 'success',
                    films: films.docs,
                    totalFilms: films.totalDocs,
                    totalPages: films.totalPages
                });
            }
            
        });
    },// end getfilmsByGenre

    //obtener resultados de la busqueda
    search: function(req, res){
        //sacamos parametro string a buscar
        var searchStr = req.params.searchString;

        //buscar por ... 
        Film.find({ "$or": [
                { "title" : { "$regex" : searchStr, "$options": "i"} },
                { "synopsis" : { "$regex" : searchStr, "$options": "i"} },
                { "genre" : { "$regex" : searchStr, "$options": "i"} },
                { "colection" : { "$regex" : searchStr, "$options": "i"} },
                { "director" : { "$regex" : searchStr, "$options": "i"} },
                { "actor" : { "$regex" : searchStr, "$options": "i"} }
            ]})
        .sort([['title', 1]])
        .exec((err, films)=>{
            if(err ){
                return res.status (500).send({
                    status: 'error',
                    message: 'Error al buscar peliculas ',
                    err
                });
            }
        
            if(!films){
                return res.status (404).send({
                    status: 'error',
                    message: 'No hay peliculas a mostrar con esa busqueda',
                });
            }else{ 
                //respuesta satisfactoria
                return res.status (200).send({
                    status: 'success',
                    films
                });
            }
        }) 
    }//end search

};//end controller

module.exports = controller;

