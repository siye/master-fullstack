'use strict'

//libreria para validar datos  DOC: https://www.npmjs.com/package/validator
const validator = require('validator');
//importamos modelos
const Activity = require('../models/activity.js');
const User = require('../models/user');

var controller = {
	//Prueba
    pruebaGet: function(req, res){
        return res.status (200).send({
            status: 'succes',
            message: 'Metodo GET en actividad'
        });  
    },

    //Prueba2
    pruebaPost: function(req, res){
        return res.status (200).send({
            status: 'succes',
            message: 'Metodo POST en actividad'
        });  
    },

    //create activity
    save: function(req, res){
    	
    	//get params
    	var params = req.body;
    	//console.log( params );
    	
    	//validations
    	var valid_user = !validator.isEmpty(params.user);
        var valid_action = !validator.isEmpty(params.action);
        var valid_complement = !validator.isEmpty(params.complement);
        
        if (!valid_action){
            return res.status(200).send({
                message: 'Validacion de la actividad incorrecta, por favor revíselos. ', params
            }); 
        }else{
        	
            //create object
            var activity = new Activity();
            
            //values to objects 
            activity.user = params.user;
            activity.action = params.action;

            if (valid_complement) {
                try{
                    activity.complement = params.complement;
                }catch(err){
                    return res.status (404).send({
                        message: 'Complemento de la actividad no válida.'
                    });
                }
            }else{
                 activity.complement = " ";
            }

	        //guardar actividad
            activity.save((err, activitySaved) => {
               
                if (err){
                    return res.status (500).send({
                        message: 'ERROR: al guardar actividad.',
                        error: err.toString(),
                        activity: activitySaved
                    });
                }
                if(!activitySaved){
                    return res.status (400).send({
                        message: 'No se ha guardado la actividad.',
                    });
                }
                //devolver respuesta
                return res.status (200).send({
                    status: 'success',
                    activity: activitySaved
                });
            });//save
            
	    }
    },// end function save 

    //update activity
    update: function(req, res){
        //recoger id activity de la url
        var actvivityId = req.params.id;

        //recogemos parametros para actualizar
        var params = req.body;

        //actualizamos activity con los datos recogidos por post
        if (!validator.isEmpty(params.state)) {
                params.state = req.body.state;
        }

        //find and update de la actividad mediante su id
        Activity.findOneAndUpdate({ _id:actvivityId }, params, {new:true}, (err, activityUpdated) =>{
            if (err){
                return res.status (500).send({
                    message: 'ERROR: error en la peticion de actualizar actividad.',
                    status: 'error',
                    error: err.toString(),
                });
            }
            if(!activityUpdated){
                return res.status (404).send({
                    status: 'error',
                    message: 'No se ha actualizado la actividad.',
                });
            }else{
                //devolver respuesta
                return res.status (200).send({
                    status: 'success',
                    activityUpdated 
                });
            }
            
        } )
    },// end function update

    //listar 500 ultimas (+ nuevas) actividades 
    getActivities: function(req, res){
    	Activity.find().sort([['_id', -1]]).limit(500)
 			.populate('user')
    		.exec((err, activities) => {
           
            if(err){
                return res.status (500).send({
                    status: 'error',
                    message: 'Error al consultar actividades',
                    err
                });
            }
        
            if(!activities){
                return res.status (404).send({
                    status: 'error',
                    message: 'No hay actividades a mostrar.'
                });
            }else{ 
                //respuesta satisfactoria (ultimas 12 pelis , pelis)
                return res.status (200).send({
                    status: 'success',
                    activities
                });
            }   
        });

    }, //end List activities

};//end controller

module.exports = controller;