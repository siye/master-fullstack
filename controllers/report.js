'use strict'

//libreria para validar datos  DOC: https://www.npmjs.com/package/validator
const validator = require('validator');
//importamos modelos
const Report = require('../models/report.js');
const User = require('../models/user');
const Film = require('../models/film');

var controller = {

	//Prueba
    pruebaPost: function(req, res){
        return res.status (200).send({
            status: 'succes',
            message: 'Metodo post en actividad'
        });  
    },
	
    //create activity
    save: function(req, res){
    	console.log( "hola" );
    	//get params
    	var params = req.body;
    	console.log( params );

    	//validations
    	var valid_user = !validator.isEmpty(params.user);
    	var valid_film = !validator.isEmpty(params.film);
        var valid_action = !validator.isEmpty(params.action);
        
        if (!valid_action){
            return res.status(200).send({
                message: 'Validacion del reporte incorrecta, por favor revíselos. ', params
            }); 
        }else{
        	
            //create object
            var report = new Report();
            
            //values to objects 
            report.user = params.user;
            report.film = params.film;
            report.action = params.action;

	        //guardar actividad
            report.save((err, reportSaved) => {
               
                if (err){
                    return res.status (500).send({
                        message: 'ERROR: al guardar el reporte.',
                        error: err.toString(),
                        report: reportSaved
                    });
                }
                if(!reportSaved){
                    return res.status (400).send({
                        message: 'No se ha guardado el reporte.',
                    });
                }
                //devolver respuesta
                return res.status (200).send({
                    status: 'success',
                    report: reportSaved
                });
            });//save
            
	    }
	
    
    },// end function save

	//update state report
    updateReport: function(req, res){

        //recoger id report de la url
        var reportId = req.params.id;

        //recogemos parametro state para actualizar
        var params = req.body;
        
        //actualizamos peli con los datos recogidos por post, vacios NO se modifican
        if (!validator.isEmpty(params.state)) {
                params.state = req.body.state;
        }
        
        //find and update de la peli mediante su id
        Report.findOneAndUpdate({ _id:reportId }, params, {new:true}, (err, reportUpdated) =>{
            if (err){
                return res.status (500).send({
                    message: 'ERROR: error en la peticion de actualizar report.',
                    status: 'error',
                    error: err.toString(),
                });
            }
            if(!reportUpdated){
                return res.status (404).send({
                    status: 'error',
                    message: 'No se ha actualizado el report.',
                });
            }else{
                //devolver respuesta
                return res.status (200).send({
                    status: 'success',
                    reportUpdated 
                });
            }
            
        } )
        
    },//end update

    //lista resportes activos, 50 registros, ordenados desc
    getReports: function(req, res){
    	Report.find({state: 'A'})
    		.sort([['_id', -1]])
    		.limit(50)
    		.populate('user')
 			.populate('film')
    		.exec((err, reports) => {
           
            if(err){
                return res.status (500).send({
                    status: 'error',
                    message: 'Error al consultar reportes',
                    err
                });
            }
        
            if(!reports){
                return res.status (404).send({
                    status: 'error',
                    message: 'No hay reportes a mostrar.'
                });
            }else{ 
            	// reports state b or c (borrado o completado), 200 registros order desc

            		Report.find({state: { $nin: [ 'A' ] }})
		    		.sort([['_id', -1]])
		    		.limit(200)
		    		.populate('user')
		 			.populate('film')
		    		.exec((err, othersReports) => {
		    			if(err){
			                return res.status (500).send({
			                    status: 'error',
			                    message: 'Error al consultar reportes',
			                    err
			                });
			            }

			            if(!reports){
			                return res.status (404).send({
			                    status: 'error',
			                    message: 'No hay reportes a mostrar.'
			                });
			            }else{ 
			            	//respuesta satisfactoria (ultimas 12 pelis , pelis)
			                return res.status (200).send({
			                    status: 'success',
			                    reports, othersReports
			                });
			            }
		    		})
                
            }   
        });

    }//end List reportes

};//end controller

module.exports = controller;