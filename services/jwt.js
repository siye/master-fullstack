'use strict'

var jwt = require ('jwt-simple');
var moment = require ('moment');

//generamos token para usuario dado, 30 dias de caducidad
exports.createToken = function(user){
    //datos del token
    var payload = {
        sub: user._id,
        email: user.email,
        nick: user.nick,
        age: user.age,
        sex: user.sex,
        role: user.role,
        iat: moment().unix(),
        exp:moment().add(30, 'days').unix()
    };
    return jwt.encode(payload, 'clave-secreta-para-generar-token-1988')
};
