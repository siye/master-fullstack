'use strict'

var jwt = require ('jwt-simple');
var moment = require ('moment');
var secret = 'clave-secreta-para-generar-token-1988'; //misma clave usada que za generar token en services/jwt.js

//token xa pruebas  20/12/2019
//  eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1ZGZhOGFkOGM0MTg4ODJkZTRmNzhmMzUiLCJlbWFpbCI6ImNvcnJlb0Bjb3JyZW8uY29tIiwibmljayI6ImFhYTEyMyIsImFnZSI6MTQsInNleCI6IkgiLCJyb2xlIjoidXNlciIsImlhdCI6MTU3Njg2Mzc4NiwiZXhwIjoxNTc5NDU1Nzg2fQ.o2e8JswB7gBT7oyT1UbEuPFx27v-QssOUQYZV1OrGoM

//funcionalidad intermedia para el update: comprobamos que antes de autentificar un usuario este ya esté logueado.
//comprobamos el token 
exports.authenticated = function(req, res, next){
    //comprobar si llega Authorización en la peticion
    if(!req.headers.authorization){
        return res.status (403).send({
            message: 'La petición no tiene la cabecera de Autorización.'
        });
    }
    //recoger token y quitarle comillas
    var token = req.headers.authorization.replace(/['"]+/g, ''); 
    //comprobar token
    try{
        //decodificar token
        var payload = jwt.decode(token, secret);
        //comprobar si el token ha expirado
        if(payload <= moment().unix()){
            return res.status (404).send({
                message: 'Token ha expirado. Vuelva a loguearse para generar uno nuevo.'
            });
        }
    }catch(ex){
        return res.status (404).send({
            message: 'Token no válido.'
        });
    }
    //adjuntar usuario identificado a la request
    req.user = payload;
    //continuamos (pasamos a la accion que llamo al middleware) 
    next();

};
