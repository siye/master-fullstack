'use strict'

//Requires
var express = require('express');
var bodyParser = require('body-parser');

//Eject Express
var app = express(); 
//cors npm  PRUEBA
//const cors = require('cors');

//charge routes files
var user_routes = require('./routes/user');
var film_routes = require('./routes/film');
var activity_routes = require('./routes/activity');
var report_routes = require('./routes/report');

//Middlewares  (funciones que se ejecutan antes de llegar a las acciones de los controladores)
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

//CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header("Access-Control-Allow-Credentials", true);
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE, HEAD, PATCH');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, content-type, application/json, Access-Control-Allow-Request-Method');
    next();
});

//Rewrite routes
app.use('/api', user_routes);
app.use('/api', film_routes);
app.use('/api', activity_routes);
app.use('/api', report_routes);


//export module
module.exports = app;