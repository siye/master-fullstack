'use strict'

var mongoose = require ('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var Schema = mongoose.Schema;

var FilmSchema = Schema({
    title: String,
    synopsis: String,
    genre: String,
    long: String,
    year: String,
    colection: String,
    director: String,
    actor: String,
    trailer: String,
    img: String,
    uri: String,
    uri2: String,
    uri3: String,
    uri4: String,
    uri5: String
    //seen: Number
});
//cargamos paginacion
FilmSchema.plugin(mongoosePaginate);
//crea la tabla - lowercase +  pluraliza el nombre
module.exports = mongoose.model ('Film', FilmSchema);