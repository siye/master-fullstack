'use strict'

var mongoose = require ('mongoose');
var Schema = mongoose.Schema;

var ReportSchema = Schema({
	user: { type: Schema.ObjectId, ref: 'User'} , //populamos user 
    film: { type: Schema.ObjectId, ref: 'Film'} , //populamos peli 
    date: {type: Date, default: Date.now}, 
    action: String,
    state: {type: String , default:'A'},
});

//crea la tabla - lowercase +  pluraliza el nombre --> activities
module.exports = mongoose.model ('Report', ReportSchema);

//el documento se crea al realizar una accion  (al guardar la primera actividad se creara )