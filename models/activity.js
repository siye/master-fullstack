'use strict'

var mongoose = require ('mongoose');
var Schema = mongoose.Schema;

var ActivitySchema = Schema({
    user: { type: Schema.ObjectId, ref: 'User'} , //populamos user 
    date: {type: Date, default: Date.now}, 
    action: String,
    complement: String,
});

//crea la tabla - lowercase +  pluraliza el nombre --> activities
module.exports = mongoose.model ('Activity', ActivitySchema);

//el documento se crea al realizar una accion  (al guardar la primera actividad se creara )