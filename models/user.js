
'use strict'

var mongoose = require ('mongoose');
var Schema = mongoose.Schema;

var UserSchema = Schema({
    name: String,
    surname: String,
    nick: String,
    email: String,
    password: String,
    bornDate: Date,
    phone: String,
    role: String
});

//crea la tabla - lowercase +  pluraliza el nombre --> users
module.exports = mongoose.model ('User', UserSchema);