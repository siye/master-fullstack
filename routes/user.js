'use strict'

var express = require('express');
var UserController = require('../controllers/user');


var router = express.Router();
//middleware para update (comprobamos token)
var md_auth = require('../middlewares/authenticated');


//rutas de la app
router.post('/registro', UserController.save);
router.post('/login', UserController.login);
router.put('/actualizar', md_auth.authenticated, UserController.update);
router.get('/usuarios', UserController.getUsers);
router.get('/user/:userId', UserController.getUser);

module.exports = router;