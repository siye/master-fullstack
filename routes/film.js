'use strict'

var express = require('express');
var FilmController = require('../controllers/film');

var router = express.Router();
var md_auth = require('../middlewares/authenticated');

//rutas de la app
router.post('/new', FilmController.save);
//router.get('/films/:page?', FilmController.getFilms);
router.get('/films', FilmController.getFilms);
router.get('/film/:id', FilmController.getFilm);
router.get('/genres', FilmController.getGenres);
router.get('/genre/:genre', FilmController.getfilmsByGenre);
router.get('/genre/:genre/:page?', FilmController.getfilmsByGenre);
router.put('/update/:id', md_auth.authenticated, FilmController.update);
router.delete('/delete/:id', md_auth.authenticated, FilmController.delete);
router.get('/search/:searchString', FilmController.search);

module.exports = router;