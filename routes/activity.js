'use strict'

var express = require('express');
var ActivityController = require('../controllers/activity.js');

var router = express.Router();

router.post('/pruebaPost', ActivityController.pruebaPost);
router.get('/pruebaGet', ActivityController.pruebaGet);
router.post('/addActivity', ActivityController.save);
router.get('/activities',  ActivityController.getActivities);

module.exports = router;