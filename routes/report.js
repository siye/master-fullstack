'use strict'

var express = require('express');
var ReportController = require('../controllers/report.js');

var router = express.Router();


router.post('/addReport', ReportController.save);
router.get('/reports',  ReportController.getReports);
router.put('/updateReport/:id',ReportController.updateReport);


module.exports = router;