'use strict'

const mongoose = require('mongoose');
const app = require('./app');
const port = process.env.PORT || 3999

mongoose.set('useFindAndModify', false); //quitar un warning de mongoose
mongoose.Promise = global.Promise;

//Mongo db Local: 'mongodb://localhost:27017/api_rest_node'   
mongoose.connect('mongodb://siye:randall2@ds257648.mlab.com:57648/heroku_fxzh9mjw', { useUnifiedTopology: true, useNewUrlParser: true }) 
    .then(()=>{
        //create server (with express)
        app.listen(port, () =>{
            console.log("El servidor http://localhost:3999 está en funcionamiento");
        }); 
    })
    .catch(error => console.log(error));


